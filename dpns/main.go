package main

import (
	"encoding/json"
	"flag"
	"fmt"
	//"io/ioutil"
	"log"
	"net/http"
    "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	//"strings"
)

// error response contains everything we need to use http.Error
type handlerError struct {
	Error   error
	Message string
	Code    int
}

// logs model
type Logs struct{
    Push_Code string `bson:"push_code" json:"push_code"`
    App_ID string `bson:"app_id"`
    User_ID string `bson:"user_id"`
    Device_ID string `bson:"device_id"`
    Date_Access string `bson:"date_access"`
    Message string `bson:"message"`
    IP_Address string `bson:"ip_address"`
    Status string `bson:"status" bson:"status"`
    Id     int    `json:"id"`
}

type Applications struct {
    ID bson.ObjectId `bson:"_id,omitempty"`
    Name string `bson:"name"`
    App_Version string `bson:"app_version"`
    Key_Access string `bson:"key_access"`
    GCM_Access_Key string `bson:"gcm_access_key"`
    Baidu_Access_Key string `bson:"baidu_access_key"`
    APNS_PEM string `bson:"apns_pem"`
    APNS_PEM_NoEnc string `bson:"apns_pem_noenc"`
    Id     int    `json:"id"`
}

type Devices struct {
    ID bson.ObjectId `bson:"_id,omitempty"`
    User_ID string `bson:"user_id"`
    Device_ID string `bson:"device_id"`
    Push_Type string `bson:"push_type"`
    Device_Model string `bson:"device_model"`
    Device_OS string `bson:"device_os"`
    Id     int    `json:"id"`
}

type Users struct {
    ID bson.ObjectId `bson:"_id,omitempty"`
    User_ID string `bson:"user_id"`
    App_ID string `bson:"app_id"`
    Phone_Number string `bson:"phone_number"`
    Id     int    `json:"id"`
}

var logs = make([]Logs, 0)
var apps = make([]Applications, 0)
var devices = make([]Devices, 0)
var users = make([]Users, 0)

// a custom type that we can use for handling errors and formatting responses
type handler func(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)

// attach the standard ServeHTTP method to our handler so the http library can call it
func (fn handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// here we could do some prep work before calling the handler if we wanted to

	// call the actual handler
	response, err := fn(w, r)

	// check for errors
	if err != nil {
		log.Printf("ERROR: %v\n", err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s"}`, err.Message), err.Code)
		return
	}
	if response == nil {
		log.Printf("ERROR: response from method is nil\n")
		http.Error(w, "Internal server error. Check the logs.", http.StatusInternalServerError)
		return
	}

	// turn the response into JSON
	bytes, e := json.Marshal(response)
	if e != nil {
		http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
		return
	}

	//fmt.Printf(string(bytes))

	// send the response and log
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
	log.Printf("%s %s %s %d", r.RemoteAddr, r.Method, r.URL, 200)
}

func listLogs(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	//connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("logs")
        
        if err != nil {
                log.Fatal(err)
        }

        result := Logs{}
        iter := c.Find(bson.M{}).Iter()
       
       	var loop int;
       	loop=1
       	logs=nil
        for iter.Next(&result){
            logs = append(logs, Logs{result.Push_Code, result.App_ID,result.User_ID,result.Device_ID, result.Date_Access, result.Message, result.IP_Address, result.Status,getNextId()})
            loop++
        }	


	return logs, nil
}

func listApplications(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	//connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("applications")
        
        if err != nil {
                log.Fatal(err)
        }

        result := Applications{}
        iter := c.Find(bson.M{}).Iter()
       
       	var loop int;
       	loop=1
       	apps=nil
        for iter.Next(&result){
			apps = append(apps, Applications{result.ID, result.Name,result.App_Version,result.Key_Access, result.GCM_Access_Key, result.Baidu_Access_Key, result.APNS_PEM, result.APNS_PEM_NoEnc,getNextId()})
            loop++
        }	
	return apps, nil
}

func listDevices(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	//connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("devices")
        
        if err != nil {
                log.Fatal(err)
        }

        result := Devices{}
        iter := c.Find(bson.M{}).Iter()
       
       	var loop int;
       	loop=1
       	devices=nil
        for iter.Next(&result){
			devices = append(devices, Devices{result.ID, result.User_ID,result.Device_ID,result.Push_Type, result.Device_Model, result.Device_OS,getNextId()})
            loop++
        }	
	return devices, nil
}

func listUsers(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	//connecting to mongoDB
        session, err := mgo.Dial("localhost:27017")
        if err != nil {
                panic(err)
        }

        defer session.Close()

        // Optional. Switch the session to a monotonic behavior.
        session.SetMode(mgo.Monotonic, true)

        //convenient access
        c := session.DB("dummydata2").C("users")
        
        if err != nil {
                log.Fatal(err)
        }

        result := Users{}
        iter := c.Find(bson.M{}).Iter()
       
       	var loop int;
       	loop=1
       	users=nil
        for iter.Next(&result){
			users = append(users, Users{result.ID, result.User_ID,result.App_ID,result.Phone_Number,getNextId()})
            loop++
        }	
	return users, nil
}


var id = 0

// increments id and returns the value
func getNextId() int {
	id += 1
	return id
}

func main() {
	// command line flags
	port := flag.Int("port", 80, "port to serve on")
	dir := flag.String("directory", "web/", "directory of web files")
	flag.Parse()

	// handle all requests by serving a file of the same name
	fs := http.Dir(*dir)
	fileHandler := http.FileServer(fs)

	// setup routes
	router := mux.NewRouter()
	router.Handle("/", http.RedirectHandler("/static/", 302))
	router.Handle("/logs", handler(listLogs)).Methods("GET")
	router.Handle("/apps", handler(listApplications)).Methods("GET")
	router.Handle("/devices", handler(listDevices)).Methods("GET")
	router.Handle("/users", handler(listUsers)).Methods("GET")
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static", fileHandler))
	http.Handle("/", router)
	
	log.Printf("Running on port %d\n", *port)

	addr := fmt.Sprintf("127.0.0.1:%d", *port)
	// this call blocks -- the progam runs here forever
	err := http.ListenAndServe(addr, nil)
	fmt.Println(err.Error())
}